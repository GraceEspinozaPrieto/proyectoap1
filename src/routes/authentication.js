//rutas login
const express = require('express');
const router = express.Router();

const passport = require('passport');
const {isLoggedIn} = require('../lib/auth');

//redenlarizar un formulario
router.get('/signup', (req, res) =>{
    res.render('auth/signup');
});

 //recibir los datos de ese formulario

router.post('/signup', passport.authenticate('local.signup', {
    successRedirect:'/signin',
    failureRedirect: '/signup',
    failureFlash: true 
}));

router.get('/signin', (req, res) => {
    res.render('auth/signin');
});
//aqui se recibira la informacion
router.post('/signin', (req, res, next) =>{
    passport.authenticate('local.signin', {
        successRedirect: '/profile',
        failureRedirect: '/signin',
        failureFlash: true
    }) (req, res, next);
});
router.get('/profile',isLoggedIn,(req, res) => {
    res.render('profile');
});

router.get('/logout',(req,res)=>{
   req.logOut();
   res.redirect('/signin');
});
module.exports = router;